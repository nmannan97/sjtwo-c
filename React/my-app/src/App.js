//import React from 'react';
import logo from './logo.svg';
import './App.css';
import React, { Component } from 'react';
import Textbox from './textbox';

//var string_data = document.getElementById('textBox').value;
//document.getElementById("output").innerHTML = string_data;

class App extends Component {
  render(){
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>Enter an address location, city, state</p>
          <Textbox />
        </header>
        
        
 
      </div>
    );
  }
}
/*
<p> My token = {window.token}</p>
<p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
*/
export default App;
