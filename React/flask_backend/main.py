import flask
import googlemaps
import bluetooth
import struct

gmaps = googlemaps.Client(key = 'AIzaSyCK16wTwgKVNO6cYhURe59ruqrmrWa68aA')

app = flask.Flask("__main__")

@app.route('/')

def my_index():
    return flask.render_template('index.html', token = 'hello world')

#example location '4913 portmarnoch ct, California, San Jose'
@app.route('/result', methods = ['GET'])
def input_data():
    location = flask.request.args.get('location', None)
    
    driver_socket = bluetooth.BluetoothSocket(bluetooth.RFCOMM)

    nearby_devices = bluetooth.discover_devices(lookup_names = True)

    for name, addr in nearby_devices:
        if("HC" in addr):
            for i in range(14):
                try:
                    driver_socket.connect((name,i))
                except OSError:
                    continue
            
            
    try:
        address = location
        coor = gmaps.geocode(address)
        lat = coor[0]["geometry"]["location"]['lat']
        lng = coor[0]["geometry"]["location"]['lng']
        driver_socket.send(str(lat))
        driver_socket.send(":")
        driver_socket.send(str(lng))
        driver_socket.close()
    except IndexError:
        driver_socket.close()
        return "Address not available" 
    return {'lat':lat, 'lng':lng}


app.run(debug = True)