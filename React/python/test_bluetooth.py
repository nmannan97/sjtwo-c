import bluetooth
import serial
import subprocess

driver_socket = bluetooth.BluetoothSocket(bluetooth.RFCOMM)

nearby_devices = bluetooth.discover_devices(lookup_names = True)

print ("found %d devices" % len(nearby_devices))


for name, addr in nearby_devices:
    if("HC" in addr):
        #print(name)
        #subprocess.call("kill -9 `pidof bluetooth-agent`",shell=True)
        #status = subprocess.call("bluetooth-agent " + str(1234) + " &",shell=True)
        for i in range(14):
            try:
                driver_socket.connect((name,i))
            except OSError:
                continue
        driver_socket.send("Success!")
        driver_socket.close()

   