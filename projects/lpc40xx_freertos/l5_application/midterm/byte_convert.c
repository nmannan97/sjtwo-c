//.h file
#include "byte_convert.h"

// stdint
#include <stdint.h>

void byte_convert__to_network_from_uint64(uint8_t bytes[8], uint64_t source_value) {
  // uint8_t *p = (uint8_t *)&source_value;
  for (uint8_t i = 0; i < sizeof(bytes); i++) {
    bytes[i] = (source_value << 2);
  }
}
void byte_convert__to_network_from_uint32(uint8_t bytes[4], uint32_t source_value) {
  uint8_t *p = (uint8_t *)&source_value;
  for (uint8_t i = 0; i < sizeof(bytes); i++) {
    bytes[i] = p[i];
  }
}
void byte_convert__to_network_from_uint16(uint8_t bytes[2], uint16_t source_value) {
  uint8_t *p = (uint8_t *)&source_value;
  for (uint8_t i = sizeof(bytes) - 1; i != 0; i--) {
    bytes[i] = (source_value >> 2 * i);
  }
}