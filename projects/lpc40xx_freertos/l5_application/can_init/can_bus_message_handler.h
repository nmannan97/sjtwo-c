// can_bus_message_handler.h

#include "can_bus.h"

void can_bus_message_handler__transmit_test(void) {
  can__msg_t msg = {0};
  msg.msg_id = 0x123;
  msg.frame_fields.is_29bit = 0;
  msg.frame_fields.data_len = 8;       // Send 8 bytes
  msg.data.qword = 0x1122334455667788; // Write all 8 bytes of data at once

  can__tx(can1, &msg, 100);
}
void can_bus_message_handler__rx(void) {
  // TODO: Send a message periodically
  can__msg_t msg = {0};

  // Empty all messages received in a 100Hz slot
  while (can__rx(can1, &msg, 100)) {
    // uart_printf(UART__0, "id: %u, qword: %lu", msg.msg_id, msg.data.qword);
  }
}