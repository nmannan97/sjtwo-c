// can_bus_initalizer.c

#include "can_bus_initializer.h"
#include <stdio.h>

void bus_off_callback(uint32_t icr_value) {
  (void)icr_value;

  puts("bus off cb called");
}

void data_overrun_callback(uint32_t icr_value) {
  (void)icr_value;

  puts("bus data overrun cb called");
}
void can_bus_init(void) {
  // TODO: You should refactor this initialization code to dedicated "can_bus_initializer.h" code module
  // Read can_bus.h for more details
  can__init(can1,                   // CAN setup
            500,                    // 500Kbps
            128,                    // Que size = 2 frames
            128,                    // Queue size, tx = 2 frames
            bus_off_callback,       // function to say if cb is off
            data_overrun_callback); // Data overrun
  can__bypass_filter_accept_all_msgs();
  can__reset_bus(can1);
}