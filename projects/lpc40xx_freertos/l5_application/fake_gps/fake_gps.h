#pragma once

/// Initialize fake_gps
void fake_gps__init(void);

/// Send a fake GPS string
void fake_gps__run_once(void);
