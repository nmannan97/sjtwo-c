#pragma once

typedef struct {
  float latitude;
  float longitude;
} bt_coordinates_t;

void bt__init(void);

void bt__run_once(void);

void bt__check(void);

bt_coordinates_t bt__get_coordinates(void);