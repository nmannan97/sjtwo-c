// TODO:
// test_gps.c
#include "unity.h"

// Mocks
#include "Mockclock.h"
#include "Mockgpio.h"
#include "Mockuart.h"

#include "Mockqueue.h"

// Use the real implementation (not mocks) for:
#include "line_buffer.h"

// Include the source we wish to test
#include "gps.c"

void setUp(void) {
  memset(&parsed_coordinates, 0, sizeof(parsed_coordinates));

  gpio_s txd3 = {0};
  gpio_s rxd3 = {0};

  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_4, 28, GPIO__FUNCTION_2, txd3); // P4.28 as TXD3
  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_4, 29, GPIO__FUNCTION_2, rxd3); // P4.29 as RXD3

  clock__get_peripheral_clock_hz_IgnoreAndReturn(0);
  uart__init_Expect(gps_uart, 0, 38400U);
  uart__init_IgnoreArg_peripheral_clock();

  xQueueCreate_ExpectAndReturn(100, sizeof(char), NULL);
  xQueueCreate_ExpectAndReturn(8, sizeof(char), NULL);

  uart__enable_queues_ExpectAndReturn(gps_uart, NULL, NULL, NULL);
  uart__enable_queues_IgnoreArg_queue_receive();
  uart__enable_queues_IgnoreArg_queue_transmit();
  gps__init();
}

void tearDown(void) {}

void test_gps_init(void) {}

void test_gps__line_buffer_and_gps_data(void) {
  const char gps_data[] = "$GPGGA,230612.015,3907.3815,N,12102.4634,W,0,04,5.7,508.3,M,,,,0000*13\n";

  for (size_t push_count = 0; push_count < strlen(gps_data); push_count++) {
    TEST_ASSERT_TRUE(line_buffer__add_byte(&line, gps_data[push_count]));
  }
  TEST_ASSERT_EQUAL(line_buffer__get_item_count(&line), strlen(gps_data));

  char gps_line[100];
  const char gps_data_no_new_line[] = "$GPGGA,230612.015,3907.3815,N,12102.4634,W,0,04,5.7,508.3,M,,,,0000*13";
  TEST_ASSERT_TRUE(line_buffer__remove_line(&line, gps_line, sizeof(gps_line)));
  TEST_ASSERT_EQUAL_STRING(gps_line, gps_data_no_new_line);
}

void test_gps__handle_line(void) {
  const char gps_data1[] = "$GPGGA,230612.015,3907.3815,N,12102.4634,W,0,04,5.7,508.3,M,,,,0000*13\n";

  for (size_t push_count = 0; push_count < strlen(gps_data1); push_count++) {
    TEST_ASSERT_TRUE(line_buffer__add_byte(&line, gps_data1[push_count]));
  }
  TEST_ASSERT_EQUAL(line_buffer__get_item_count(&line), strlen(gps_data1));

  gps__handle_line();
  // TODO use gps__get_coordinates() function instread of private members
  TEST_ASSERT_EQUAL_FLOAT(parsed_coordinates.latitude, 3907.3815);
  TEST_ASSERT_EQUAL_FLOAT(parsed_coordinates.longitude, -12102.4634);
  TEST_ASSERT_EQUAL(line_buffer__get_item_count(&line), 0);

  const char gps_data2[] = "$GPGGA,230611.016,3907.3813,S,12102.4635,W,0,04,5.7,507.9,M,,,,0000*11\n";

  for (size_t push_count = 0; push_count < strlen(gps_data2); push_count++) {
    TEST_ASSERT_TRUE(line_buffer__add_byte(&line, gps_data2[push_count]));
  }
  TEST_ASSERT_EQUAL(line_buffer__get_item_count(&line), strlen(gps_data2));

  gps__handle_line();
  TEST_ASSERT_EQUAL_FLOAT(parsed_coordinates.latitude, -3907.3813);
  TEST_ASSERT_EQUAL_FLOAT(parsed_coordinates.longitude, -12102.4635);
  TEST_ASSERT_EQUAL(line_buffer__get_item_count(&line), 0);

  const char gps_data3[] = "$GPGGA,120557.916,5058.7456,N,00647.0515,E,2,06,1.7,108.5,M,47.6,M,1.5,0000*7A\n";

  for (size_t push_count = 0; push_count < strlen(gps_data3); push_count++) {
    TEST_ASSERT_TRUE(line_buffer__add_byte(&line, gps_data3[push_count]));
  }
  TEST_ASSERT_EQUAL(line_buffer__get_item_count(&line), strlen(gps_data3));

  gps__handle_line();
  TEST_ASSERT_EQUAL_FLOAT(parsed_coordinates.latitude, 5058.7456);
  TEST_ASSERT_EQUAL_FLOAT(parsed_coordinates.longitude, 647.0515);
  TEST_ASSERT_EQUAL(line_buffer__get_item_count(&line), 0);

  const char gps_data4[] = "$GPGGA,120558.916,5058.7457,S,00647.0514,W,2,06,1.7,109.0,M,47.6,M,1.5,0000*71\n";

  for (size_t push_count = 0; push_count < strlen(gps_data4); push_count++) {
    TEST_ASSERT_TRUE(line_buffer__add_byte(&line, gps_data4[push_count]));
  }
  TEST_ASSERT_EQUAL(line_buffer__get_item_count(&line), strlen(gps_data4));

  gps__handle_line();
  TEST_ASSERT_EQUAL_FLOAT(parsed_coordinates.latitude, -5058.7457);
  TEST_ASSERT_EQUAL_FLOAT(parsed_coordinates.longitude, -647.0514);
  TEST_ASSERT_EQUAL(line_buffer__get_item_count(&line), 0);
}