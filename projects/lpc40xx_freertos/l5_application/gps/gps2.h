// gps.h
#pragma once

typedef struct {
  float latitude;
  float longitude;
} gps_coordinates_t;

// Initializes GPS module
void gps__init2(void);

// Parses GPS lines in line buffer and sets coordinates
void gps__run_once2(void);

// Returns set coordinates from gps__run_once() function
gps_coordinates_t gps__get_coordinates2(void);
