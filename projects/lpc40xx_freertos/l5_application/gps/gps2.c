// gps.c
#include "gps2.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

// GPS module dependency
#include "gpio.h"
#include "line_buffer.h"
#include "uart.h"

// Rtos dependency for the UART driver
#include "FreeRTOS.h"
#include "queue.h"

#include "clock.h" // needed for UART initialization

// Change this according to which UART you plan to use
static uart_e gps_uart = UART__3;

// Space for the line buffer, and the line buffer data structure instance
static char line_buffer[256];
static line_buffer_s line;
// Single object for storing parsed coordinates from uart
static gps_coordinates_t parsed_coordinates;

/// Private functions:

// Fills line buffer from uart receive queue
static void gps__absorb_data(void) {
  char byte;
  while (uart__get(gps_uart, &byte, 0)) {
    line_buffer__add_byte(&line, byte);
  }
}

// Checks if passed GPS line is GPGGA data
static bool gps__parsed_line_is_GPGGA_data(const char *gps_line) {
  const char GGA_string_header[] = "$GPGGA";
  return memcmp(GGA_string_header, gps_line, strlen(GGA_string_header)) == 0;
}

// Converts waypoints to degrees
static float gps__convert_waypoint_to_degree(const float waypoint) {
  bool is_negative = false;
  float degree_conversion = waypoint;

  if (degree_conversion < 0) {
    is_negative = true;
    degree_conversion *= -1;
  }

  // Waypoints follows (d)ddmm.mmmm
  // d is degrees and m is decimal minutes
  const size_t extracted_degrees_from_waypoint = degree_conversion / 100;
  degree_conversion =
      extracted_degrees_from_waypoint + (degree_conversion - (extracted_degrees_from_waypoint * 100)) / 60;

  if (is_negative) {
    degree_conversion *= -1;
  }

  return degree_conversion;
}

// Parses GPGGA fixed latitude values and sets
// parsed_coordinates.latitude along with respective polarity
static void gps__parse_and_set_latitude(const char *gps_line, const uint8_t latitude_start_index,
                                        const uint8_t latitude_orientation_index) {
  // An additional index included from comma between latitude and orientation serves as null terminator
  char latitude_string[latitude_orientation_index - latitude_start_index];

  memcpy((void *)latitude_string, gps_line + latitude_start_index, sizeof(latitude_string) - 1);
  parsed_coordinates.latitude = atof(latitude_string);

  if (gps_line[latitude_orientation_index] == 'S') {
    parsed_coordinates.latitude *= -1;
  }

  parsed_coordinates.latitude = gps__convert_waypoint_to_degree(parsed_coordinates.latitude);
}

// Parses GPGGA fixed longitude values and sets
// parsed_coordinates.longitude along with respective polarity
static void gps__parse_and_set_longitude(const char *gps_line, const uint8_t longitude_start_index,
                                         const uint8_t longitude_orientation_index) {
  // An additional index included from comma between longitude and orientation serves as null terminator
  char longitude_string[longitude_orientation_index - longitude_start_index];

  memcpy((void *)longitude_string, gps_line + longitude_start_index, sizeof(longitude_string) - 1);
  parsed_coordinates.longitude = atof(longitude_string);

  if (gps_line[longitude_orientation_index] == 'W') {
    parsed_coordinates.longitude *= -1;
  }

  parsed_coordinates.longitude = gps__convert_waypoint_to_degree(parsed_coordinates.longitude);
}

/**
  Global Positioning System Fix Data. Time, position and fix related data for a GPS receiver.

  $GPGGA,hhmmss.sss,llll.llll,a,yyyyy.yyyy,a,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx*hh

  $GPGGA = Header
  hhmmss.sss = UTC of position
  llll.llll = latitude of position
  a = N or S
  yyyyy.yyyy = Longitude of position
  a = E or W
  x = GPS Quality indicator (0=no fix, 1=GPS fix, 2=Dif. GPS fix)
  xx = number of satellites in use
  x.x = horizontal dilution of precision
  x.x = Antenna altitude above mean-sea-level
  M = units of antenna altitude, meters
  x.x = Geoidal separation
  M = units of geoidal separation, meters
  x.x = Age of Differential GPS data (seconds)
  xxxx = Differential reference station ID
  hh = Checksum and always begin with *

  This internal function assumes that latitude and longitude are positions in NMEA record.
  Bit fields follow NavMan 3400 (SiRF chipset sentences) output where latitude starts after the second comma,
  latitude orientation starts after the third comma, longitude starts after the fourth comma, and longitude
  orientation starts after the fifth comma. Orientation of latitude/longitude is applied through polarity.

  IMPORTANT: All bit fields are fixed and zero filled to meet bit field length. Bit fields follow
             NavMan 3400 (SiRF chipset sentences) output.
*/
static void gps__handle_line(void) {
  char gps_line[100];
  if (line_buffer__remove_line(&line, gps_line, sizeof(gps_line))) {
    puts(gps_line);
    if (gps__parsed_line_is_GPGGA_data(gps_line)) {
      uint8_t latitude_start_index = 0;
      uint8_t latitude_orientation_index = 0;

      uint8_t longitude_start_index = 0;
      uint8_t longitude_orientation_index = 0;

      uint8_t comma_count = 0;
      for (uint8_t index = 0; index < strlen(gps_line); index++) {
        if (',' == gps_line[index]) {
          comma_count++;

          switch (comma_count) {
          case 2:
            latitude_start_index = index + 1;
            break;
          case 3:
            latitude_orientation_index = index + 1;
            break;
          case 4:
            longitude_start_index = index + 1;
            break;
          case 5:
            longitude_orientation_index = index + 1;
            break;
          default:
            break;
          }
        }

        if (comma_count == 5) {
          gps__parse_and_set_latitude(gps_line, latitude_start_index, latitude_orientation_index);
          gps__parse_and_set_longitude(gps_line, longitude_start_index, longitude_orientation_index);
          break;
        }
      }
    }
  }
}

/// Public functions:

void gps__init2(void) {
  gpio__construct_with_function(GPIO__PORT_4, 28, GPIO__FUNCTION_2); // P4.28 as TXD3
  gpio__construct_with_function(GPIO__PORT_4, 29, GPIO__FUNCTION_2); // P4.29 as RXD3

  line_buffer__init(&line, line_buffer, sizeof(line_buffer));
  uart__init(gps_uart, clock__get_peripheral_clock_hz(), 115200);

  // RX queue should be sized such that it can buffer data in UART driver until gps__run_once() is called
  // Note: Assuming 115200bps, we can get 115200 / 11 bits = ~10.5 chars per ms; thus ~105 characters per 10 ms @ 100 Hz
  // which is perfect for NavMan 3400 (SiRF chipset sentences) GPS lines which are about ~72 characters in length
  QueueHandle_t rxq_handle = xQueueCreate(128, sizeof(char));
  QueueHandle_t txq_handle = xQueueCreate(8, sizeof(char)); // We don't send anything to the GPS
  uart__enable_queues(gps_uart, rxq_handle, txq_handle);
}

void gps__run_once2(void) {
  gps__absorb_data();
  gps__handle_line();
}

gps_coordinates_t gps__get_coordinates2(void) { return parsed_coordinates; }
